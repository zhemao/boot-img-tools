#include "bootimg.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int read_section(FILE *bootimg_fp, FILE *section_fp, 
		 int section_size, int page_size)
{
	int bytes_read = 0, bytes_written;
	char page[page_size];
	
	while (bytes_read < section_size) {
		if (fread(page, page_size, 1, bootimg_fp) < 1)
			return -1;

		bytes_read += page_size;

		if (bytes_read > section_size)
			bytes_written = section_size % page_size;
		else
			bytes_written = page_size;

		if (fwrite(page, bytes_written, 1, section_fp) < 1)
			return -1;
	}

	return 0;
}

int seek_past_section(FILE *bootimg_fp, int section_size, int page_size)
{
	int seek_len = section_size;

	if (section_size % page_size > 0)
		seek_len += (page_size - (section_size % page_size));
	
	return fseek(bootimg_fp, seek_len, SEEK_CUR);
}

int main(int argc, char *argv[])
{
	int opt, bootimg_ind, kernel_ind, ramdisk_ind, second_ind;
	FILE *bootimg_fp, *kernel_fp, *ramdisk_fp, *second_fp;
	struct boot_img_hdr header;
	char header_magic[9];
	int seek_len = 0;
	int err;

	bootimg_ind = kernel_ind = ramdisk_ind = second_ind = 0;
	bootimg_fp = kernel_fp = ramdisk_fp = second_fp = NULL;

	while ((opt = getopt(argc, argv, "b:k:r:s:")) != -1) {
		switch (opt) {
		case 'b':
			bootimg_ind = optind - 1;
			break;
		case 'k':
			kernel_ind = optind - 1;
			break;
		case 'r':
			ramdisk_ind = optind - 1;
			break;
		case 's':
			second_ind = optind - 1;
			break;
		default:
			fprintf(stderr, "Usage: %s {-b boot.img}"
					"[-k kernel.img] [-r ramdisk.gz]"
					"[-s second.img]\n", argv[0]);
			exit(EXIT_FAILURE);
		}
	}

	if (bootimg_ind == 0) {
		fprintf(stderr, "Must supply boot image\n");
		return EXIT_FAILURE;
	}

	bootimg_fp = fopen(argv[bootimg_ind], "r");
	if (bootimg_fp == NULL) {
		perror("open file\n");
		return EXIT_FAILURE;
	}
	
	if (fread(&header, sizeof(header), 1, bootimg_fp) < 1) {
		fprintf(stderr, "Could not read header\n");
		fclose(bootimg_fp);
		return EXIT_FAILURE;
	}

	memcpy(header_magic, header.magic, 8);
	header_magic[9] = '\0';
	if (strcmp(header_magic, BOOT_MAGIC) != 0) {
		fprintf(stderr, "Not a valid boot image\n");
		fclose(bootimg_fp);
		return EXIT_FAILURE;
	}

	printf("Name: %s\n", header.name);
	printf("Command Line: %s\n", header.cmdline);
	printf("Page size: %u\n", header.page_size);
	printf("Kernel size/addr: %u/%x\n", header.kernel_size, 
					    header.kernel_addr);
	printf("Ramdisk size/addr: %u/%x\n", header.ramdisk_size,
					     header.ramdisk_addr);
	printf("Second stage size/addr: %u/%x\n", header.second_size,
						  header.second_addr);
	printf("Tags Addr: %x\n", header.tags_addr);

	seek_len = header.page_size - sizeof(header);
	if (fseek(bootimg_fp, seek_len, SEEK_CUR) < 0) {
		fprintf(stderr, "Could not seek to kernel\n");
		fclose(bootimg_fp);
		return EXIT_FAILURE;
	}

	if (kernel_ind) {
		kernel_fp = fopen(argv[kernel_ind], "w");
		if (kernel_fp == NULL) {
			perror("open file\n");
			fclose(bootimg_fp);
			return EXIT_FAILURE;
		}

		err = read_section(bootimg_fp, kernel_fp, 
				 header.kernel_size, header.page_size);
		fclose(kernel_fp);
		if (err < 0) {
			fprintf(stderr, "Error reading kernel section\n");
			fclose(bootimg_fp);
			return EXIT_FAILURE;
		}
	} else if (header.kernel_size) {
		if (seek_past_section(bootimg_fp, header.kernel_size,
				      header.page_size) < 0) {
			fprintf(stderr, "Error skipping past kernel section\n");
			fclose(bootimg_fp);
			return EXIT_FAILURE;
		}
	}
	
	if (ramdisk_ind) {
		ramdisk_fp = fopen(argv[ramdisk_ind], "w");
		if (ramdisk_fp == NULL) {
			perror("open file\n");
			fclose(bootimg_fp);
			return EXIT_FAILURE;
		}

		err = read_section(bootimg_fp, ramdisk_fp,
				   header.ramdisk_size, header.page_size);
		fclose(ramdisk_fp);
		if (err < 0) {
			fprintf(stderr, "Error reading ramdisk section\n");
			fclose(bootimg_fp);
			return EXIT_FAILURE;
		}
	} else if (header.ramdisk_size) {
		if (seek_past_section(bootimg_fp, header.ramdisk_size,
				      header.page_size) < 0) {
			fprintf(stderr, "Error skipping past ramdisk section\n");
			fclose(bootimg_fp);
			return EXIT_FAILURE;
		}
	}
	
	if (second_ind) {
		second_fp = fopen(argv[second_ind], "w");
		if (second_fp == NULL) {
			perror("open file\n");
			fclose(bootimg_fp);
			return EXIT_FAILURE;
		}

		err = read_section(bootimg_fp, second_fp,
				   header.second_size, header.page_size);
		fclose(second_fp);
		if (err < 0) {
			fprintf(stderr, "Error reading second section\n");
			fclose(bootimg_fp);
			return EXIT_FAILURE;
		}
	} else if (header.second_size) {
		if (seek_past_section(bootimg_fp, header.second_size,
				      header.page_size) < 0) {
			fprintf(stderr, "Error skipping past second section\n");
			fclose(bootimg_fp);
			return EXIT_FAILURE;
		}
	}

	return EXIT_SUCCESS;
}
