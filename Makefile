CFLAGS=-g -Wall -I.
MINCRYPT_FILES=mincrypt/rsa.o mincrypt/rsa_e_3.o mincrypt/rsa_e_f4.o mincrypt/sha.o

all: mkbootimg splitimg

install: mkbootimg splitimg easymkbootimg
	cp mkbootimg ~/bin/mkbootimg
	cp splitimg ~/bin/splitimg
	cp easymkbootimg ~/bin/easymkbootimg

mkbootimg: mkbootimg.o $(MINCRYPT_FILES)
	$(CC) $(LDFLAGS) $< $(MINCRYPT_FILES) -o $@

splitimg: splitimg.o
	$(CC) $(LDFLAGS) $< -o $@

%.o: %.c
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -f mkbootimg splitimg *.o mincrypt/*.o

.PHONY: clean
